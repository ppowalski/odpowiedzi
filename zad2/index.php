<?php

/**
 * @param string $pesel
 * @return bool
 */
function validPesel(string $pesel): bool
{
     // check length and data type
     if (empty(preg_match('/[0-9]{11}/', $pesel))) {
          return false;
     }

     // check valid date
     $year = (int)substr($pesel, 0, 2);
     $month = (int)substr($pesel, 2, 2);
     $day = (int)substr($pesel, 4, 2);

     if ($month > 80 && $month <= 92) { // 1800 - 1899
          $year += 1800;
          $month -= 80;
     } elseif ($month > 0 && $month <= 12) { // 1900 - 1999
          $year += 1900;
     } elseif ($month > 20 && $month <= 32) { // 2000 - 2099
          $year += 2000;
          $month -= 20;
     } else {
          return false;
     }

     if (!checkdate($month, $day, $year)) {
          return false;
     }

     // check math valid
     $weights = [1, 3, 7, 9, 1, 3, 7, 9, 1, 3];
     $sum = 0;

     for ($i = 0; $i < 10; $i++) {
          $sum += ((int)$pesel[$i] * $weights[$i]);
     }

     $controllSum = (10 - ($sum % 10)) % 10;

     if ((int)$controllSum === (int)$pesel[10]) {
          return true;
     }

     return false;
}