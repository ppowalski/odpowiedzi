<?php
require 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;

final class PeselTest extends TestCase
{
     public function testValidPesel(): void
     {
          require_once 'index.php';

          $this->assertTrue(validPesel('89110373349'));
     }

     public function testInvalidLengthPesel(): void
     {
          require_once 'index.php';

          $this->assertTrue(validPesel('123321'));
     }

     public function testInvalidDatePesel(): void
     {
          $this->assertTrue(validPesel('00000000000'));
     }

     public function testInvalidSumPesel(): void
     {
          $this->assertTrue(validPesel('12345678911'));
     }
}