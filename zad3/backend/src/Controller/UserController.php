<?php

namespace App\Controller;

use App\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
     /**
      * @Route("/", name="home")
      */
     public function index()
     {
          return $this->render('base.html.twig');
     }

     /**
      * @Route("/users", name="getUsers", methods={"GET"})
      */
     public function getUsers(Request $request)
     {
          $username = $request->query->get('search', null);

          $users = $this->getDoctrine()
               ->getRepository(Users::class)
               ->getUsers($username);


          return $this->json(['success' => true, 'users' => $users]);
     }

     /**
      * @Route("/user", name="userCreate", methods={"POST"})
      */
     public function createUser(Request $request)
     {
          $username = $request->request->get('username', null);

          if(empty($username)) {
               return $this->json(['success' => false]);
          }

          $users = new Users();
          $users->setName($username);

          $entityManager = $this->getDoctrine()->getManager();
          $entityManager->persist($users);
          $entityManager->flush();

          return $this->json(['success' => true]);
     }

     /**
      * @Route("/user", name="editUser", methods={"PUT"})
      */
     public function editUser(Request $request)
     {
          (int)$id = $request->request->get('id', null);
          (string)$username = $request->request->get('username', null);

          if(empty($username) || empty($id)) {
               return $this->json(['success' => false]);
          }

          $user = $this->getDoctrine()
               ->getRepository(Users::class)
               ->find($id);

          if(empty($user)) {
               return $this->json(['success' => false]);
          }

          $user->setName($username);

          $entityManager = $this->getDoctrine()->getManager();
          $entityManager->flush();

          return $this->json(['success' => true]);
     }

     /**
      * @Route("/user/{id}", name="deleteUser", methods={"DELETE"})
      */
     public function deleteUser(int $id)
     {
          if(empty($id)) {
               return $this->json(['success' => false]);
          }

          $user = $this->getDoctrine()
               ->getRepository(Users::class)
               ->find($id);

          $user->setDelete();

          $entityManager = $this->getDoctrine()->getManager();
          $entityManager->flush();

          return $this->json(['success' => true]);
     }
}
