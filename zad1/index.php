<?php

/**
 * @param string $pathToFile
 * @param string $pathToDestination
 * @return bool
 */
function changeLetterInFile(string $pathToFile, string $pathToDestination): bool
{
     if (file_exists($pathToFile)) {
          copy($pathToFile, $pathToDestination);

          if (file_exists($pathToDestination)) {
               $fileContent = file_get_contents($pathToDestination);

               $fileContentArray = explode(' ', $fileContent);
               foreach ($fileContentArray as $key => $fileWord) {
                    $fileWord = trim($fileWord);
                    $firstChar = $fileWord[0];
                    $lastChar = $fileWord[strlen($fileWord) - 1];

                    $fileWord = preg_replace("/{$firstChar}/", '', $fileWord, 1);
                    $fileWord = preg_replace("/{$lastChar}/", '', $fileWord, 1);

                    $fileWord = str_shuffle($fileWord);

                    $fileContentArray[$key] = $firstChar . $fileWord . $lastChar;
               }

               $fileContent = implode(' ', $fileContentArray);
               $result = file_put_contents($pathToDestination, $fileContent);

               if (!empty($result)) {
                    return true;
               }
          }
     }

     return false;
}

var_dump(changeLetterInFile('file1.txt', 'fileCopy.txt'));